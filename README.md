# KupferDigital - Copper Ontology
Home of the domain ontology of the KupferDigital Project.

## Documentation

The ontology documentation is reachable at 
[![Documentation](https://gitlab.com/kupferdigital/process-graphs/lightmicroscopy/badges/main/pipeline.svg?style=flat-square&ignore_skipped=true&key_text=Documentation&key_width=100)](https://kupferdigital.gitlab.io/ontologies/kdo/index.html).

It also supports various serialisation formats which are also linked there.

## Contribution
This section is about the rules u have to apply to enties you want to Contribution to the ontology.

### Mandatory Annotations for Ontology Terms
- **IRIs (Internationalized Resource Identifiers)** - Ensure unique and persistent IRIs for each term

- **Classes**
  - **Naming Convention**: Use UpperCamelCase (e.g., `YourSuperNewTerm`).
  - **rdfs:label**: 
    - **Format**: Entitled
    - **Example**: "Your Super New Term"
    - **Language**: Specify languages (e.g.,`en` for English, `de` for German).
  - **Processes**: Use Gerund (e.g., `Cutting`)

- **Properties**
  - **Naming Convention**: Use lowerCamelCase (e.g., `yourNewProperty`).
  - **rdfs:label**: 
    - **Format**: Natural language
    - **Example**: "your new property"
    - **Language**: Specify languages (e.g.,`en` for English, `de` for German).

- Annotation Properties
  - **skos:definition**: 
    - **Content**: Aristotelian principle definition of the term
    - **Language**: `en` for English
  - **iao:IAO_0000114 (has curation status)**: Choose according to editing status.
  - **iao:IAO_0000117 (term editor)**: 
    - **Format**: "PERSON: Firstname Lastname" (as responsible person)

 - Additional Annotations
    - **skos:altLabel**: 
      - Use for synonyms.
      - Ensure skos:prefLabel is also set.
      - **Format**: Capitalized
    - **skos:example**: Include examples if possible.
    - **iao:IAO_0000119 (definition source)**: 
      - Use when definition is adopted from a resource.
    - **iao:IAO_0000412 (imported from)**: 
      - Use when definition is imported from existing ontology concept.
    - Additional translations are welcome.


    ## KDO features

    - subclass hierarchy of manufacturing and characterization procedures unique to the value chain of copper products.
