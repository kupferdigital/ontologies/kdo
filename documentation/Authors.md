| Name                   | ORCID               | Affiliation (acronym)                                 | [ROR][ROR] |
| ---------------------- | ------------------- | ----------------------------------------------------- | ---------- |
| Thomas Hanke           | 0000-0003-1649-6832 | Fraunhofer Institute for Mechanics of Materials IWM   | 04hm8eb66  |


[ROR]: <https://ror.org/search>
